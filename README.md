# Serber economy website

## Setting up

Requires MariaDB and Node.js with `mariadb`, `ejs`, `express` and `body-parser` (run `npm install mariadb express ejs body-parser` in the `app` directory).

To set up the database, launch `mysql` and do `source mariadb/setup.sql;`; the database can be launched afterwards with `mysql serber`. Launch the app with `node app/app.js`.

For testing use `mysql serber -h 127.0.0.1 -u <user> -p` (make sure to set up the user and password), then load `http://localhost:3000`.
