/**********************************************************************
The purpose of this file is to act as a safety mechanism incase someone
tampers with the HTML elements of the website.

This file stores constant values which stores the table values that
need to be altered by a certain function                             
**********************************************************************/

/* We usually check if a table item is a valid integer. However in some
cases it is a string value. Store all of them here so there isn't a
need for a bunch of if clauses everywhere.                             */
const TABLE_STRINGS =
[
    'military_losses.description', 'nations.name', 'flags.url', 'trade.description'
]

/* This absolutely fucking sucks. But for the time being, do it.     */
const LAND_DEPLOYMENT =
[
    'military_deployment.infantry.north_america',
    'military_deployment.artillery.north_america',
    'military_deployment.tanks.north_america',
    'military_deployment.vehicles.north_america',
    'military_deployment.aircraft.north_america',
    'military_deployment.infantry.west_coast',
    'military_deployment.artillery.west_coast',
    'military_deployment.tanks.west_coast',
    'military_deployment.vehicles.west_coast',
    'military_deployment.aircraft.west_coast',
    'military_deployment.infantry.central_america',
    'military_deployment.artillery.central_america',
    'military_deployment.tanks.central_america',
    'military_deployment.vehicles.central_america',
    'military_deployment.aircraft.central_america',
    'military_deployment.infantry.caribbean',
    'military_deployment.artillery.caribbean',
    'military_deployment.tanks.caribbean',
    'military_deployment.vehicles.caribbean',
    'military_deployment.aircraft.caribbean',
    'military_deployment.infantry.north_pacific',
    'military_deployment.artillery.north_pacific',
    'military_deployment.tanks.north_pacific',
    'military_deployment.vehicles.north_pacific',
    'military_deployment.aircraft.north_pacific',
    'military_deployment.infantry.south_pacific',
    'military_deployment.artillery.south_pacific',
    'military_deployment.tanks.south_pacific',
    'military_deployment.vehicles.south_pacific',
    'military_deployment.aircraft.south_pacific',
    'military_deployment.infantry.the_arctic',
    'military_deployment.artillery.the_arctic',
    'military_deployment.tanks.the_arctic',
    'military_deployment.vehicles.the_arctic',
    'military_deployment.aircraft.the_arctic',
    'military_deployment.infantry.the_antarctic',
    'military_deployment.artillery.the_antarctic',
    'military_deployment.tanks.the_antarctic',
    'military_deployment.vehicles.the_antarctic',
    'military_deployment.aircraft.the_antarctic',
    'military_deployment.infantry.western_europe',
    'military_deployment.artillery.western_europe',
    'military_deployment.tanks.western_europe',
    'military_deployment.vehicles.western_europe',
    'military_deployment.aircraft.western_europe',
    'military_deployment.infantry.eastern_europe',
    'military_deployment.artillery.eastern_europe',
    'military_deployment.tanks.eastern_europe',
    'military_deployment.vehicles.eastern_europe',
    'military_deployment.aircraft.eastern_europe',
    'military_deployment.infantry.middle_east',
    'military_deployment.artillery.middle_east',
    'military_deployment.tanks.middle_east',
    'military_deployment.vehicles.middle_east',
    'military_deployment.aircraft.middle_east',
    'military_deployment.infantry.central_asia',
    'military_deployment.artillery.central_asia',
    'military_deployment.tanks.central_asia',
    'military_deployment.vehicles.central_asia',
    'military_deployment.aircraft.central_asia',
    'military_deployment.infantry.east_asia',
    'military_deployment.artillery.east_asia',
    'military_deployment.tanks.east_asia',
    'military_deployment.vehicles.east_asia',
    'military_deployment.aircraft.east_asia',
    'military_deployment.infantry.south_east_asia',
    'military_deployment.artillery.south_east_asia',
    'military_deployment.tanks.south_east_asia',
    'military_deployment.vehicles.south_east_asia',
    'military_deployment.aircraft.south_east_asia',
    'military_deployment.infantry.antipodes',
    'military_deployment.artillery.antipodes',
    'military_deployment.tanks.antipodes',
    'military_deployment.vehicles.antipodes',
    'military_deployment.aircraft.antipodes',
    'military_deployment.infantry.india',
    'military_deployment.artillery.india',
    'military_deployment.tanks.india',
    'military_deployment.vehicles.india',
    'military_deployment.aircraft.india',
    'military_deployment.infantry.nort_africa',
    'military_deployment.artillery.nort_africa',
    'military_deployment.tanks.nort_africa',
    'military_deployment.vehicles.nort_africa',
    'military_deployment.aircraft.nort_africa',
    'military_deployment.infantry.sub_saharan',
    'military_deployment.artillery.sub_saharan',
    'military_deployment.tanks.sub_saharan',
    'military_deployment.vehicles.sub_saharan',
    'military_deployment.aircraft.sub_saharan'
]
const NAVAL_DEPLOYMENT =
[
    'naval_deployment.small_vessels.arctic_sea',
    'naval_deployment.submarines.arctic_sea',
    'naval_deployment.destroyers.arctic_sea',
    'naval_deployment.cruisers.arctic_sea',
    'naval_deployment.battleships.arctic_sea',
    'naval_deployment.carriers.arctic_sea',
    'naval_deployment.small_vessels.north_atlantic',
    'naval_deployment.submarines.north_atlantic',
    'naval_deployment.destroyers.north_atlantic',
    'naval_deployment.cruisers.north_atlantic',
    'naval_deployment.battleships.north_atlantic',
    'naval_deployment.carriers.north_atlantic',
    'naval_deployment.small_vessels.south_atlantic',
    'naval_deployment.submarines.south_atlantic',
    'naval_deployment.destroyers.south_atlantic',
    'naval_deployment.cruisers.south_atlantic',
    'naval_deployment.battleships.south_atlantic',
    'naval_deployment.carriers.south_atlantic',
    'naval_deployment.small_vessels.baltic_sea',
    'naval_deployment.submarines.baltic_sea',
    'naval_deployment.destroyers.baltic_sea',
    'naval_deployment.cruisers.baltic_sea',
    'naval_deployment.battleships.baltic_sea',
    'naval_deployment.carriers.baltic_sea',
    'naval_deployment.small_vessels.black_sea',
    'naval_deployment.submarines.black_sea',
    'naval_deployment.destroyers.black_sea',
    'naval_deployment.cruisers.black_sea',
    'naval_deployment.battleships.black_sea',
    'naval_deployment.carriers.black_sea',
    'naval_deployment.small_vessels.mediterranean_sea',
    'naval_deployment.submarines.mediterranean_sea',
    'naval_deployment.destroyers.mediterranean_sea',
    'naval_deployment.cruisers.mediterranean_sea',
    'naval_deployment.battleships.mediterranean_sea',
    'naval_deployment.carriers.mediterranean_sea',
    'naval_deployment.small_vessels.great_lakes',
    'naval_deployment.submarines.great_lakes',
    'naval_deployment.destroyers.great_lakes',
    'naval_deployment.cruisers.great_lakes',
    'naval_deployment.battleships.great_lakes',
    'naval_deployment.carriers.great_lakes',
    'naval_deployment.small_vessels.caribbean_sea',
    'naval_deployment.submarines.caribbean_sea',
    'naval_deployment.destroyers.caribbean_sea',
    'naval_deployment.cruisers.caribbean_sea',
    'naval_deployment.battleships.caribbean_sea',
    'naval_deployment.carriers.caribbean_sea',
    'naval_deployment.small_vessels.north_pacific',
    'naval_deployment.submarines.north_pacific',
    'naval_deployment.destroyers.north_pacific',
    'naval_deployment.cruisers.north_pacific',
    'naval_deployment.battleships.north_pacific',
    'naval_deployment.carriers.north_pacific',
    'naval_deployment.small_vessels.south_pacific',
    'naval_deployment.submarines.south_pacific',
    'naval_deployment.destroyers.south_pacific',
    'naval_deployment.cruisers.south_pacific',
    'naval_deployment.battleships.south_pacific',
    'naval_deployment.carriers.south_pacific',
    'naval_deployment.small_vessels.antarctic',
    'naval_deployment.submarines.antarctic',
    'naval_deployment.destroyers.antarctic',
    'naval_deployment.cruisers.antarctic',
    'naval_deployment.battleships.antarctic',
    'naval_deployment.carriers.antarctic',
    'naval_deployment.small_vessels.indian_ocean',
    'naval_deployment.submarines.indian_ocean',
    'naval_deployment.destroyers.indian_ocean',
    'naval_deployment.cruisers.indian_ocean',
    'naval_deployment.battleships.indian_ocean',
    'naval_deployment.carriers.indian_ocean',
    'naval_deployment.small_vessels.arabian_sea',
    'naval_deployment.submarines.arabian_sea',
    'naval_deployment.destroyers.arabian_sea',
    'naval_deployment.cruisers.arabian_sea',
    'naval_deployment.battleships.arabian_sea',
    'naval_deployment.carriers.arabian_sea',
    'naval_deployment.small_vessels.coastal_asia',
    'naval_deployment.submarines.coastal_asia',
    'naval_deployment.destroyers.coastal_asia',
    'naval_deployment.cruisers.coastal_asia',
    'naval_deployment.battleships.coastal_asia',
    'naval_deployment.carriers.coastal_asia'
]

/* All table columns required by a certain function */
const TABLE_REFERENCES = 
{
    addTrade: ['trade.gdp', 'trade.workers', 'trade.food', 'trade.coal', 'trade.iron', 'trade.oil', 'trade.rubber', 'trade.description'],
    addLoss: ['military_losses.infantry',   'military_losses.artillery',     'military_losses.tanks',    'military_losses.vehicles',
              'military_losses.aircraft',   'military_losses.small_vessels', 'military_losses.submarines', 
              'military_losses.destroyers', 'military_losses.cruisers',      'military_losses.battleships',
              'military_losses.carriers',   'military_losses.description'],
    addForeignBusiness : ['foreign_businesses.percent', 'foreign_businesses.counts'],
    addForeignResources : ['foreign_resources.food', 'foreign_resources.coal', 'foreign_resources.iron', 'foreign_resources.oil',
                           'foreign_resources.rubber'],
    saveFlag: ['nations.name', 'flags.url'],
    savePopulation: ['nations.pop_core', 'nations.pop_colonial'],
    saveBlockade: ['nations.bloc_level'],
    savePlants: ['buildings.coal_plants', 'buildings.oil_plants'],
    saveResources: ['buildings.coal_mines', 'buildings.farms', 'buildings.iron_mines', 'buildings.oil_drills', 'buildings.rubber_farms'],
    saveBuildings: ['buildings.housing', 'buildings.civil', 'buildings.businesses'],
    saveFactories: ['military_buildings.aircraft', 'military_buildings.artillery', 'military_buildings.infantry', 'military_buildings.naval',
                    'military_buildings.tank',     'military_buildings.vehicle'],
    saveNavalAlloc: ['naval_assignment.small_vessels', 'naval_assignment.submarines', 'naval_assignment.destroyers', 'naval_assignment.cruisers',
                    'naval_assignment.battleships',    'naval_assignment.carriers'],
    saveLandDep: LAND_DEPLOYMENT,
    saveNavalDep: NAVAL_DEPLOYMENT
}
/* Matching xhttp to the table above                                 */
const FUNC_REFERENCES =
{
    addTrade:               'add',
    addLoss:                'add',
    addForeignBusiness :    'add',
    addForeignResources :   'add',
    saveFlag:               'save',
    savePopulation:         'save',
    saveBlockade:           'save',
    savePlants:             'save',
    saveResources:          'save',
    saveBuildings:          'save',
    saveFactories:          'save',
    saveNavalAlloc:         'save',
    saveLandDep:            'save',
    saveNavalDep:           'save',
    removeTrade:            'remove',
    removeLoss:             'remove',
    removeForeignBusiness:  'remove',
    removeForeignResources: 'remove',
}
/* Applicable to removing: what table are they interested in?        */
const REMOVE_REFERENCES =
{
    removeTrade:            'trade',
    removeLoss:             'military_losses',
    removeForeignBusiness:  'foreign_businesses',
    removeForeignResources: 'foreign_resources'
}

/* Public APIs */
function isReference(func){
    if (TABLE_REFERENCES.hasOwnProperty(func)) {
        return true
    }
    return false
}

function isHttp(func){
    if (FUNC_REFERENCES.hasOwnProperty(func)) {
        return true
    }
    return false
}

function getHttp(func){
    if (!isHttp(func)){return ""}
    return FUNC_REFERENCES[func]
}

function getRemove(func){
    return REMOVE_REFERENCES[func]
}

function isInTable(func, item){
    /* TODO: Inefficient O(n^2) solution. */
    if (!isReference(func)){return false}
    for (let column of TABLE_REFERENCES[func]){
        if (item == column){return true}
    }
    return false
}

function getTable(func){
    if (!isReference(func)){return ""}
    return TABLE_REFERENCES[func]
}

function isString(item){
    for (let i = 0; i < TABLE_STRINGS.length; i++) { 
        if (TABLE_STRINGS[i] == item){return true}
    }
    return false
}

/* Exporting */
module.exports = Object.freeze({
    isReference: isReference,
    isHttp: isHttp,
    getHttp: getHttp,
    getRemove: getRemove,
    isInTable: isInTable,
    getTable: getTable,
    isString: isString
})