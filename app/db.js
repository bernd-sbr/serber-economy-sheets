const mariadb = require('mariadb')
const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
});
  

// hilariously insecure
// idk how to make this better
var pool =
    mariadb.createPool({
        host: "127.0.0.1",
        user: "root",
        password: "admin"
    })

module.exports = Object.freeze({
    updateGdp: ((code, value) => {
        query(
            `UPDATE serber.nations
            SET GDP = ${value}
            WHERE Code = "${code}";`,
            `Set GDP for ${code} to ${value}`)
    }),
    addTrade: ((code, amount, desc) => {
        let lcode = code.toLowerCase()

        query(
            `INSERT INTO serber.nations_${lcode}
            (LocalCode, Amount, Description)
            VALUES ("${code}", ${amount}, "${desc}");`,
            `Traded in ${amount} for ${code}: "${desc}"`)

        query(
            `UPDATE serber.nations
            SET GDP = (SELECT COALESCE(SUM(Amount), 0)
                       FROM serber.nations_${lcode})
            WHERE Code = "${code}";`,
            `Updated GDP for ${code}`)
    }),
    removeTrade: ((code, id) => {
        let lcode = code.toLowerCase()

        query(
            `DELETE FROM serber.nations_${lcode}
            WHERE ID = ${id}`,
            `Removed a trade for ${code}`)

        query(
            `UPDATE serber.nations
            SET GDP = (SELECT COALESCE(SUM(Amount), 0)
                       FROM serber.nations_${lcode})
            WHERE Code = "${code}";`,
            `Updated GDP for ${code}`)
    }),
    getInfo: getInfo,
    getEconomy: getEconomy,
    getTrade: getTrade,
    getMilitary: getMilitary,
    getTableHtml: getTableHtml,
    getRaw: getRaw,
    getIndex, getIndex,
    save: save,
    add: add,
    remove: remove
})

// idk if you need to establish a connection every time
// but I'm going to abstract it because a bunch of functions use it
async function query (qr, message) {
    let conn

    try {
        conn = await pool.getConnection()

        await conn.query(qr)

        console.log(message)
    } catch (err) {
        console.log(err)
    } finally {
        if (conn) await conn.release()
    }
}


// Helper function which puts a variable name in a grammatical format
function formatName(str){
    if (!str){return ""}
    str = str.replace(/_/g, ' ')
    const sentence = str.split(" ");
    for (let i = 0; i < sentence.length; i++) {
        sentence[i] = sentence[i][0].toUpperCase() + sentence[i].substr(1);
    }
    str = sentence.join(" ");
    return str
}

// can do something like a list of ignored keys
// but cannot figure out how to make that work with
// the current switch statement setup
//var ignoredKeys = [ 'ID', 'LocalCode' ]

// returns html table with results from sql select
// receives sql query and callback function to return the table
/* TODO: returns just an empty html table if
         there are no entries which is ugly
         can be fixed by querying table columns
         which I don't know how to do yet */
<<<<<<< HEAD
async function getTableHtml(hdr, sql, cb) {
=======
async function getTableHtml(sql, cb, type="", func="") {
>>>>>>> origin/nap

    var table = '<table border="1">'

    let conn

    retlabel: try {
        conn = await pool.getConnection()

        let resql = await conn.query(sql)

        if (!resql || !resql.length) break retlabel

        keys = Object.keys(resql[0])

<<<<<<< HEAD
        table += '<tr>' + hdr + '</tr>'
=======
        table += '<tr>'

        for (let i = 0; i < keys.length; i++){
            switch (keys[i]) {
                case 'ID':
                    if (type=="trade" || type=="losses" || type=="foreignBusiness" || type == "foreignResources"){ //TODO: refactor
                        table += '<th></th>'
                    }
                    break
                case 'code': break
                case 'LocalCode': break
                default:
                    table += '<th>' + formatName(keys[i]) + '</th>'
            }
        }

        table += '</tr>'
>>>>>>> origin/nap

        for (let i = 0; i < resql.length; i++) {

            table += '<tr>'

            // this case analysis is hacky but works for now
            for (var j = 0; j < keys.length; j++)
                switch (keys[j]) {
                    case 'ID': 
                        if (type=="trade" || type=="losses" || type=="foreignBusiness" || type == "foreignResources"){ //TODO: refactor
                            table += `<td><button onClick="remove(${resql[i].ID}, &quot;${func}&quot;)" id="${resql[i].ID}">REMOVE</button></td>`
                        }
                        break

                    case 'Name':
                        table += '<td><li><a href="/nationdata/'
                              +  resql[i].Code.toLowerCase()
                              +  '">'
                              +  resql[i].Name
                              +  '</a></li></td>'
                        break

                    // had to create a new table column to keep track of the
                    // table we currently find ourselves in
                    case 'LocalCode': break

                    case 'Description':
                        let tid = resql[i].ID
                        let lcode = resql[i].LocalCode.toLowerCase()
                        let form =
                        `<form action="/nationdata/del_${lcode}"
                               method="POST">
                            <button name="rem"
                                    value="${tid}">
                                Remove
                            </button>
                        </form>`

                        table += '<td>'
                              +  resql[i].Description
                              +  '</td><td>'
                              +  form
                              +  '</td>'
                        break

                    case 'code': break
                    case 'theater':
                        table += `<td>${formatName(resql[i][keys[j]])}</td>`
                        break
                    case 'trade':
                        table += `<td>${formatName(resql[i][keys[j]])}</td>`
                        break
                    default:
                        if (type=="military_deployment" || type=="naval_deployment"){
                            if (resql[i].theater == 'unassigned'){
                                table += `<td>${resql[i][keys[j]]}</td>`
                            }
                            else{
                                table += `<td><input value=${resql[i][keys[j]]} id="${type}.${keys[j]}.${resql[i].theater}"></td>`
                            }
                            break
                        }
                        table += `<td>${resql[i][keys[j]]}</td>`
                }

            table += '</tr>'
        }
    } catch (err) {
        console.log(err)
    } finally {
        if (conn) await conn.release()
    }

    table += '</table>'
    
    return cb(table)
}

async function getRaw(sql, cb) {

    let resql
    let conn

    retlabel: try {
        conn = await pool.getConnection()

        resql = await conn.query(sql)

        if (!resql || !resql.length) break retlabel

    } catch (err) {
        console.log(err)
    } finally {
        if (conn) await conn.release()
    }    
    return cb(resql)
}

// Helper function
// I stole this from stack overflow, too lazy to write this myself
// Merge two JSON objects, keeping the values of the 2nd
function mergeJson(obj1, obj2){
    const result = {}
    let key

    for (key in obj1) {
        if(obj1.hasOwnProperty(key)){
            result[key] = obj1[key]
        }
    }

    for (key in obj2) {
        if(obj2.hasOwnProperty(key)){
            result[key] = obj2[key]
        }
    }
    return result
}

// Helper function
// Count the number of factories/ports a nation has
async function getFactoryCount(code, returnJson, cb){
    let factoryCount = 0
    let portCount    = 0
    await getRaw(`SELECT * FROM serber.military_buildings where Code="${code}";`, msg => {
        jsonObj = msg[0]
        for (const [key, value] of Object.entries(jsonObj)) {
            if (key.toLowerCase() == "code"){continue}
            else if (key.toLowerCase() == "naval"){
                portCount += parseInt(value)
            }
            else{
                factoryCount += parseInt(value)
            }
        }
    })
    returnJson.factory_count = factoryCount;
    returnJson.port_count    = portCount;
    return cb(returnJson)
}

// Function for home page
async function getIndex(cb) {
    let returnJson   = {}
    await getRaw(`SELECT code, name, gdp, url, valid FROM serber.nations NATURAL JOIN serber.flags;`, msg => {
        returnJson = msg;
    })
    //sorting based on gdp
    returnJson.sort((a,b) => Number(b.gdp) - Number(a.gdp));
    // formatting
    for (let i = 0; i < returnJson.length; i++){
        returnJson[i].gdp = formatter.format(Number(returnJson[i].gdp))
        returnJson[i].valid = (returnJson[i].valid == 0) ? "INVALID" : "VALID"
    }
    console.log(returnJson)
    return cb(returnJson);
}

// Function for "Info" page
async function getInfo(code, cb) {
    let returnJson   = {}
    // serber.nations
    await getRaw(`SELECT * FROM serber.nations WHERE Code="${code}";`, msg => {
        returnJson = msg[0];
    })
    // serber.resources
    await getRaw(`SELECT * FROM serber.resources WHERE Code="${code}";`, msg => {
        returnJson = mergeJson(returnJson, msg[0])
    })
    // serber.resources (for cap values)
    await getRaw(`SELECT u_total, u_naval FROM serber.military_buildings WHERE Code="${code}";`, msg => {
        returnJson = mergeJson(returnJson, msg[0])
    })
    // misc
    await getFactoryCount(code, returnJson, msg => {returnJson = msg})
    // getting military summary
    const militaryKeys = ['infantry', 'artillery', 'tanks', 'vehicles', 'aircraft']
    const navyKeys     = ['small_vessels', 'submarines', 'destroyers', 'cruisers', 'battleships', 'carriers']
    let   militaryJson = {}
    let   lossesJson   = {}
    for (const key of militaryKeys){
        await getRaw(`SELECT sum(${key}) as ${key} from serber.military_deployment WHERE Code="${code}";`, msg => {
            militaryJson = mergeJson(militaryJson, msg[0])
        })
        await getRaw(`SELECT sum(${key}) as ${key} from serber.military_losses WHERE Code="${code}";`, msg => {
            lossesJson = mergeJson(lossesJson, msg[0])
        })
    }
    for (const key of navyKeys){
        await getRaw(`SELECT sum(${key}) as ${key} from serber.naval_deployment WHERE Code="${code}";`, msg => {
            militaryJson = mergeJson(militaryJson, msg[0])
        })
        await getRaw(`SELECT sum(${key}) as ${key} from serber.military_losses WHERE Code="${code}";`, msg => {
            lossesJson = mergeJson(lossesJson, msg[0])
        })
    }
    for (const key of Object.keys(militaryJson)){
        Object.defineProperty(militaryJson, formatName(key),
            Object.getOwnPropertyDescriptor(militaryJson, key));
        delete militaryJson[key];
    }
    for (const key of Object.keys(lossesJson)){
        Object.defineProperty(lossesJson, formatName(key),
            Object.getOwnPropertyDescriptor(lossesJson, key));
        delete lossesJson[key];
    }
    returnJson.military_summary = militaryJson
    returnJson.losses_summary   = lossesJson
    // serber.flags
    await getRaw(`SELECT * FROM serber.flags WHERE Code="${code}";`, msg => {
        returnJson = mergeJson(returnJson, msg[0])
    })
    // formatting
    returnJson.gdp = formatter.format(Number(returnJson.gdp))
    returnJson.valid = (returnJson.valid == 0) ? "INVALID" : "VALID"
    returnJson.client_script = "/resources/client.js"
    console.log(returnJson)
    return cb(returnJson);
}

// Function for "Economy" page
async function getEconomy(code, cb) {
    let returnJson = {}
    let factoryCount = 0
    // serber.nations
    await getRaw(`SELECT * FROM serber.nations WHERE Code="${code}";`, msg => {
        returnJson = msg[0];
    })
    // serber.resources
    await getRaw(`SELECT * FROM serber.resources WHERE Code="${code}";`, msg => {
        returnJson = mergeJson(returnJson, msg[0])
    })
    // serber.buildings
    await getRaw(`SELECT * FROM serber.buildings WHERE Code="${code}";`, msg => {
        returnJson = mergeJson(returnJson, msg[0])
    })
    // serber.resources (for cap values)
    await getRaw(`SELECT u_total, u_naval FROM serber.military_buildings WHERE Code="${code}";`, msg => {
        returnJson = mergeJson(returnJson, msg[0])
    })
    // serber.foreign_businesses
    await getTableHtml(`SELECT * FROM serber.foreign_businesses WHERE Code="${code}";`, msg => {
        // Instead of merging, easier to store seperately.
        returnJson.foreign_businesses = msg
    }, "foreignBusiness", "removeForeignBusiness")
    // serber.foreign_resources
    await getTableHtml(`SELECT * FROM serber.foreign_resources WHERE Code="${code}";`, msg => {
        // Instead of merging, easier to store seperately.
        returnJson.foreign_resources = msg
    }, "foreignResources", "removeForeignResources")
    // serber.flags
    await getRaw(`SELECT * FROM serber.flags WHERE Code="${code}";`, msg => {
        returnJson = mergeJson(returnJson, msg[0])
    })
    // misc
    await getFactoryCount(code, returnJson, msg => {returnJson = msg})
    // formatting
    returnJson.gdp = formatter.format(Number(returnJson.gdp))
    returnJson.valid = (returnJson.valid == 0) ? "INVALID" : "VALID"
    returnJson.client_script = "/resources/client.js"
    console.log(returnJson)
    return cb(returnJson);
}

// Function for "Trade" page
async function getTrade(code, cb) {
    let returnJson = {}
    let factoryCount = 0
    // serber.nations
    await getRaw(`SELECT * FROM serber.nations WHERE Code="${code}";`, msg => {
        returnJson = msg[0];
    })
    // serber.resources
    await getRaw(`SELECT * FROM serber.resources WHERE Code="${code}";`, msg => {
        returnJson = mergeJson(returnJson, msg[0])
    })
    // serber.resources (for cap values)
    await getRaw(`SELECT u_total, u_naval FROM serber.military_buildings WHERE Code="${code}";`, msg => {
        returnJson = mergeJson(returnJson, msg[0])
    })
    // serber.trade (trade list)
     await getTableHtml(`SELECT * FROM serber.trade WHERE Code="${code}";`, msg => {
        // Instead of merging, easier to store seperately.
        returnJson.trade = msg
    }, "trade", "removeTrade")
    // serber.flags
    await getRaw(`SELECT * FROM serber.flags WHERE Code="${code}";`, msg => {
        returnJson = mergeJson(returnJson, msg[0])
    })
    // misc
    await getFactoryCount(code, returnJson, msg => {returnJson = msg})
    // formatting
    returnJson.gdp = formatter.format(Number(returnJson.gdp))
    returnJson.valid = (returnJson.valid == 0) ? "INVALID" : "VALID"
    returnJson.client_script = "/resources/client.js"
    console.log(returnJson)
    return cb(returnJson);
}

// Function for "Military" page
async function getMilitary(code, cb) {
    let returnJson = {}
    let factoryCount = 0
    // serber.nations
    await getRaw(`SELECT * FROM serber.nations WHERE Code="${code}";`, msg => {
        returnJson = msg[0];
    })
    // serber.resources
    await getRaw(`SELECT * FROM serber.resources WHERE Code="${code}";`, msg => {
        returnJson = mergeJson(returnJson, msg[0])
    })
    // serber.buildings
    await getRaw(`SELECT * FROM serber.buildings WHERE Code="${code}";`, msg => {
        returnJson = mergeJson(returnJson, msg[0])
    })
    // serber.military_buildings
    await getRaw(`SELECT * FROM serber.military_buildings WHERE Code="${code}";`, msg => {
        returnJson = mergeJson(returnJson, msg[0])
    })
    // serber.naval_assignment
    await getRaw(`SELECT * FROM serber.naval_assignment WHERE Code="${code}";`, msg => {
        // Instead of merging, easier to store seperately.
        returnJson.naval_assignment = msg[0]
    })
    // serber.military_deployment
    await getTableHtml(`SELECT * FROM serber.military_deployment WHERE Code="${code}";`, msg => {
        returnJson.military_deployment = msg
    }, "military_deployment")
    // serber.naval_deployment
    await getTableHtml(`SELECT * FROM serber.naval_deployment WHERE Code="${code}";`, msg => {
        returnJson.naval_deployment = msg
    }, "naval_deployment")
    // serber.military_losses (losses list)
    await getTableHtml(`SELECT * FROM serber.military_losses WHERE Code="${code}";`, msg => {
        // Instead of merging, easier to store seperately.
        returnJson.losses = msg
    }, "losses", "removeLoss")
    // serber.flags
    await getRaw(`SELECT * FROM serber.flags WHERE Code="${code}";`, msg => {
        returnJson = mergeJson(returnJson, msg[0])
    })
    // misc
    await getFactoryCount(code, returnJson, msg => {returnJson = msg})
    // formatting
    returnJson.gdp = formatter.format(Number(returnJson.gdp))
    returnJson.valid = (returnJson.valid == 0) ? "INVALID" : "VALID"
    returnJson.client_script = "/resources/client.js"
    console.log(returnJson)
    return cb(returnJson);
}

// Function for saving to database
async function save(input, code, cb) {
    let keys = Object.keys(input)
    for (let i = 0; i < keys.length; i++){
        const table   = (keys[i].split("."))[0]
        const column  = (keys[i].split("."))[1]
        const value   = input[keys[i]]
        console.log(table, column, value)

        // TODO: refactor this crap
        let sql = ""
        if (table == "flags" || (table == "nations" && column == "name")){
            sql = `UPDATE serber.${table} SET ${column}="${value}" WHERE Code="${code}"`
        }
        else{
            sql = `UPDATE serber.${table} SET ${column}=${value} WHERE Code="${code}"`
            // Stuff for deployments
            if ((keys[i].split("."))[2] != null && (table == "military_deployment" || table == "naval_deployment")){
                sql = `${sql} AND Theater="${(keys[i].split("."))[2]}"`
            }
        }
        sql = `${sql};`
        await query(sql, sql)
    }
    // Calling procedures
    await procedures()
    return cb("Successfuly saved data.")
}

// Function for adding to database
async function add(input, code, cb) {
    let keys = Object.keys(input)
    let sql1 = ``
    let sql2 = ``
    for (let i = 0; i < keys.length; i++){
        const column  = (keys[i].split("."))[1]
        let value     = input[keys[i]]
        if (column == "description"){
            value = `"${value}"`
        }
        //console.log(table, column, value)

        // TODO: refactor this crap
        if (i == 0){
            sql1 = `${column}`
            sql2 = `${value}`
        }
        else{
            sql1 = `${sql1}, ${column}`
            sql2 = `${sql2}, ${value}`
        }
    }
    // Note: this assumes this is all updating the SAME table.
    const table   = (keys[0].split("."))[0]
    const sql     = `INSERT INTO serber.${table} (code, ${sql1}) VALUES ("${code}", ${sql2});`
    await query(sql, sql)
    // Calling procedures
    await procedures()
    return cb("Successfuly saved data.")
}

// Function for removing to database
async function remove(input, code, table, cb) {
    const id  = input.id
    const sql = `DELETE FROM serber.${table} WHERE ID=${id} AND CODE="${code}"`
    await query(sql, sql)
    // Calling procedures
    await procedures()
    return cb("Successfuly saved data.")
}

// Function for procedure updates
// TODO: inefficient to do this all for every save
async function procedures(){
    let sql = `call serber.update_gdp;`
    await query(sql, sql)
    sql = `call serber.update_factory_caps;`
    await query(sql, sql)
    sql = `call serber.update_validity;`
    await query(sql, sql)
    sql = `call serber.update_resources;`
    await query(sql, sql)
    sql = `call serber.update_military_deployment;`
    await query(sql, sql)
    sql = `call serber.update_naval_deployment;`
    await query(sql, sql)
    sql = `call serber.update_naval_assignments;`
    await query(sql, sql)
}
