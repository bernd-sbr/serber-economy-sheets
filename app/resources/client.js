//WIP
// Function for updating a table
function save(func){
    // Gather each input element
    let inputs = document.getElementsByTagName('input');

    //Response body
    let body = {}
    for (i = 0; i < inputs.length; i++) {
        // Client-side check (insecure)
        if ((inputs[i].value).length > 255){
            alert(`The value for ${inputs[i].id} is too long.`)
            return;
        }
        body[inputs[i].id] = inputs[i].value
    }

    //POST XMLHttpRequest for the body
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			alert(xhttp.response);
			//Redirecting
			window.location.reload();
            return;
		}
		else if (this.readyState == 4 && this.status != 200){
			alert("Failed to save. Try again.\n" + xhttp.response);
            return;
		}
	};
	xhttp.open("POST", window.location.href + `/${func}`, true);
    xhttp.setRequestHeader('Content-Type', 'application/json')
	xhttp.send(JSON.stringify(body));
}

function remove(id, func){
    //POST XMLHttpRequest for the body
	let xhttp = new XMLHttpRequest();
    //Response body
    let body = {id: id}
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			alert(xhttp.response);
			//Redirecting
			window.location.reload();
            return;
		}
		else if (this.readyState == 4 && this.status != 200){
			alert("Failed to remove. Try again.\n" + xhttp.response);
            return;
		}
	};
	xhttp.open("POST", window.location.href + `/${func}`, true);
    xhttp.setRequestHeader('Content-Type', 'application/json')
	xhttp.send(JSON.stringify(body));
}
