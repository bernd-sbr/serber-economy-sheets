const express = require('express')
//const { body, validationResult } = require('express-validator')
const db = require('./db')
const defs = require('./defs')
const app = express()
const port = 3000

// necessary for getting info from html forms
// (otherwise `req.body` below will always be undefined)
const bodyParser = require('body-parser');

app.set('view engine','ejs')
app.set('views', __dirname + '/views')
app.use("/resources", express.static(__dirname + "/resources"))
app.use(express.json({limit: '1mb'}));
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    db.getIndex(msg => {
        res.render('index', {
            info  : msg
        })
    })
})

// nationdata page (redirect)
app.get('/nationdata', (req, res) => {
    res.redirect('/')
});
// Summary page (redirect)
app.get('/nationdata/:leg/summary', (req, res) => {
    // TODO: redundant, but could be useful incase /nationdata/:leg is used for something else
    res.redirect('/nationdata/' + req.params.leg)
});

// Summary page
app.get('/nationdata/:leg', (req, res) => {
    db.getInfo(req.params.leg, msg => {
        res.render('summary', {
            info  : msg
        })
    })
})

// Economy Page
app.get('/nationdata/:leg/economy', (req, res) => {
    db.getEconomy(req.params.leg, msg => {
        res.render('economy', {
            info  : msg
        })
    })
})

app.post('/nationdata/:leg/:func',                          updateDatabase);
app.post('/nationdata/:leg/summary/:func',                  updateDatabase);
app.post('/nationdata/:leg/economy/:func',                  updateDatabase);
app.post('/nationdata/:leg/trade/:func',                    updateDatabase);
app.post('/nationdata/:leg/military/:func',                 updateDatabase);

function updateDatabase(req, res){
    res.format({
        "application/json": function(){
            // Getting request body data
            if (req.body == null || req.params.func == null || req.params.leg == null){
                res.status(400).send(`Could not parse request body.`);
                stop = true;
                return;
            }
            console.log(req.leg)
            console.log(req.params)
            console.log(req.body)
            let newBody = {}
            let stop = false  // This is necessary since the "return" statements don't work for whatever reason.
            // Applicable to "remove" only
            if (defs.getHttp(req.params.func) == 'remove' && req.body.hasOwnProperty("id")){   // If we are removing, "body" only has the "id" key, which we need.
                newBody = req.body;
            }
            else{
                // Parsing data we're interested in
                Object.entries(req.body).forEach((entry) => {
                    let [key, value] = entry;
                    const table = key.split(".")[0]
                    const column = key.split(".")[1]
                    // Error checking
                    if (table == null || column == null){
                        res.status(400).send(`Could not parse data.`);
                        stop = true;
                        return;
                    }
                    if (key.length > 255){
                        res.status(400).send(`Too long of a value.`);
                        stop = true;
                        return;
                    }
                    if (!defs.isReference(req.params.func) || !defs.isHttp(req.params.func)){
                        res.status(400).send(`Could not find database reference.`);
                        stop = true;
                        return;
                    }
                    if (defs.isInTable(req.params.func, key)){             // Filtering for the SQL tables we are going to alter.
                        newBody[key] = value
                    }
                });
                // Checking if the data is valid
                Object.entries(newBody).forEach((entry) => {
                    let [key, value] = entry;
                    const column = key.split(".")[1]
                    if (!defs.isString(key)){
                        value = parseFloat(value)
                        if (!Number.isInteger(value)){
                            res.status(400).send(`${column} is not an integer value.`);
                            stop = true;
                            return;
                        }
                        if (value < 0 && !key.startsWith('trade')){
                            res.status(400).send(`${column} must be a positive integer.`);
                            stop = true;
                            return;
                        }
                        // This is arbritrary, could just use the integer ceiling, but it prevents a crash.
                        if (value > 9999 || (value > 100 && key == 'foreign_businesses.percent')){
                            res.status(400).send(`${column} is too high of a value.`);
                            stop = true;
                            return;
                        }
                    }
                });
            }
            if (!stop){
                // Client data is valid, update database
                switch (defs.getHttp(req.params.func)){
                    case 'save':
                        db.save(newBody, req.params.leg, msg => {
                            res.status(200).send(msg);
                        })
                        break;
                    case 'add':
                        db.add(newBody, req.params.leg, msg => {
                            res.status(200).send(msg);
                        })
                        break;
                    case 'remove':
                        removeTable = defs.getRemove(req.params.func)
                        db.remove(newBody, req.params.leg, removeTable, msg => {
                            res.status(200).send(msg);
                        })
                        break;
                    default:
                        res.status(400).send(`An unknown error has occurred.`);
                        break;
                }
            }
        }
    });
}

// Trade Page
app.get('/nationdata/:leg/trade', (req, res) => {
    db.getTrade(req.params.leg, msg => {
        res.render('trade', {
            info  : msg
        })
    })
})

// Military Page
app.get('/nationdata/:leg/military', (req, res) => {
    db.getMilitary(req.params.leg, msg => {
        res.render('military', {
            info  : msg
        })
    })
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})

