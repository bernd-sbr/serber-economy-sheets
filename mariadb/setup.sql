drop database if exists serber;

create database if not exists serber;

use serber;

/* table definition ======================================================== */
set @factories_max = 30;
set @factories_min = 5;
set @pop_min = 9;
set @pop_max = 66;
set @factory_scaling =
    (@factories_max-@factories_min)/(@pop_max-@pop_min) + 0.01;

set @infantry_factories_min = 2;
set @infantry_factories_max = 15;
set @infantry_scaling =
    @infantry_factories_max/@pop_max + 0.01;

create table if not exists nations(
    code         varchar(3)   not null unique,
    name         varchar(255) not null,
    gdp          bigint       not null default 0,
    pop_core     bigint       not null default 0,
    pop_colonial bigint       not null default 0,
    bloc_level   tinyint      not null default 0,
    valid        boolean      not null default true,
    primary key (code)
);

create table if not exists flags(
    code varchar(3),
    url  varchar(255) default '',
    primary key (code)
);

create table if not exists buildings(
    code varchar(3),
    coal_plants  int not null default 0,
    oil_plants   int not null default 0,
    farms        int not null default 0,
    coal_mines   int not null default 0,
    iron_mines   int not null default 0,
    oil_drills   int not null default 0,
    rubber_farms int not null default 0,
    housing      int not null default 0,
    civil        int not null default 0,
    businesses   int not null default 0,
    primary key (code)
);

create table if not exists resources(
    code varchar(3),
    workers int not null default 0,
    food    int not null default 0,
    coal    int not null default 0,
    iron    int not null default 0,
    oil     int not null default 0,
    rubber  int not null default 0,
    primary key (code)
);

create table if not exists trade(
<<<<<<< HEAD
    code varchar(3) not null,
    gdp     int,
    workers int,
    food    int,
    coal    int,
    iron    int,
    oil     int,
    rubber  int,
    description varchar(64)
);

create table if not exists foreign_businesses(
    code varchar(3) not null,
    percent tinyint unsigned not null,
    counts  tinyint unsigned not null
=======
    ID int not null auto_increment,
    code varchar(3) not null,
    gdp     int not null default 0,
    workers int not null default 0,
    food    int not null default 0,
    coal    int not null default 0,
    iron    int not null default 0,
    oil     int not null default 0,
    rubber  int not null default 0,
    description varchar(64) not null default '',
    primary key (ID)
);

create table if not exists foreign_businesses(
    ID int not null auto_increment,
    code varchar(3) not null,
    percent tinyint unsigned not null,
    counts  tinyint unsigned not null,
    primary key (ID)
>>>>>>> origin/nap
);

/* should be no greater than 10 */
create table if not exists foreign_resources(
<<<<<<< HEAD
=======
    ID int not null auto_increment,
>>>>>>> origin/nap
    code varchar(3) not null,
    food    tinyint unsigned,
    coal    tinyint unsigned,
    iron    tinyint unsigned,
    oil     tinyint unsigned,
<<<<<<< HEAD
    rubber  tinyint unsigned
=======
    rubber  tinyint unsigned,
    primary key (ID)
>>>>>>> origin/nap
);

create table if not exists military_buildings(
    code varchar(3),
    u_total    int not null default 0,
    u_infantry int not null default 0,
    u_naval    int not null default 0,
    infantry   int not null default 0,
    artillery  int not null default 0,
    tank       int not null default 0,
    vehicle    int not null default 0,
    aircraft   int not null default 0,
    naval      int not null default 0,
    primary key (code)
);

create table if not exists military_losses(
<<<<<<< HEAD
    code varchar(3) not null,
    infantry      int,
    artillery     int,
    tanks         int,
    vehicles      int,
    aircraft      int,
    small_vessels int,
    submarines    int,
    destroyers    int,
    cruisers      int,
    battleships   int,
    carriers      int
=======
    ID int not null auto_increment,
    code varchar(3) not null,
    infantry      int not null default 0,
    artillery     int not null default 0,
    tanks         int not null default 0,
    vehicles      int not null default 0,
    aircraft      int not null default 0,
    small_vessels int not null default 0,
    submarines    int not null default 0,
    destroyers    int not null default 0,
    cruisers      int not null default 0,
    battleships   int not null default 0,
    carriers      int not null default 0,
    description varchar(64),
    primary key (ID)
>>>>>>> origin/nap
);

create table if not exists naval_assignment(
    code varchar(3),
    unassigned    int not null default 0,
    small_vessels int not null default 0,
    submarines    int not null default 0,
    destroyers    int not null default 0,
    cruisers      int not null default 0,
    battleships   int not null default 0,
    carriers      int not null default 0,
    primary key (code)
);

create table if not exists military_deployment(
    code varchar(3) not null,
    theater   enum('unassigned',
                   'north_america',
                   'west_coast',
                   'central_america',
                   'caribbean',
                   'north_pacific',
                   'south_pacific',
                   'the_arctic',
                   'the_antarctic',
                   'western_europe',
                   'eastern_europe',
                   'middle_east',
                   'central_asia',
                   'east_asia',
                   'south_east_asia',
                   'antipodes',
                   'india',
                   'nort_africa',
                   'sub_saharan')    
                  not null,
    infantry  int not null default 0,
    artillery int not null default 0,
    tanks     int not null default 0,
    vehicles  int not null default 0,
    aircraft  int not null default 0,
    unique(code, theater)
);

create table if not exists naval_deployment(
    code varchar(3) not null,
    theater       enum('unassigned',
                       'arctic_sea',
                       'north_atlantic',
                       'south_atlantic',
                       'baltic_sea',
                       'black_sea',
                       'mediterranean_sea',
                       'great_lakes',
                       'caribbean_sea',
                       'north_pacific',
                       'south_pacific',
                       'antarctic',
                       'indian_ocean',
                       'arabian_sea',
                       'coastal_asia')
                      not null,
    small_vessels int not null default 0,
    submarines    int not null default 0,
    destroyers    int not null default 0,
    cruisers      int not null default 0,
    battleships   int not null default 0,
    carriers      int not null default 0,
    unique(code, theater)
);

/* functions =============================================================== */
delimiter $$

create or replace procedure initialize_statics()
begin
    declare cc varchar(3);
    declare done int default false;
    declare cur cursor for select code from nations;
    declare continue handler for not found set done = true;

    open cur;

    fetch_loop: loop
        fetch cur into cc;

        insert ignore into buildings(code)          values (cc);
        insert ignore into resources(code)          values (cc);
        insert ignore into military_buildings(code) values (cc);
        insert ignore into naval_assignment(code)   values (cc);
<<<<<<< HEAD
=======
        insert ignore into flags(code)              values (cc);
>>>>>>> origin/nap

        insert ignore into military_deployment(code, theater)
        values
            (cc, 'unassigned'     ),
            (cc, 'north_america'  ),
            (cc, 'west_coast'     ),
            (cc, 'central_america'),
            (cc, 'caribbean'      ),
            (cc, 'north_pacific'  ),
            (cc, 'south_pacific'  ),
            (cc, 'the_arctic'     ),
            (cc, 'the_antarctic'  ),
            (cc, 'western_europe' ),
            (cc, 'eastern_europe' ),
            (cc, 'middle_east'    ),
            (cc, 'central_asia'   ),
            (cc, 'east_asia'      ),
            (cc, 'south_east_asia'),
            (cc, 'antipodes'      ),
            (cc, 'india'          ),
            (cc, 'nort_africa'    ),
            (cc, 'sub_saharan'    );
        insert ignore into naval_deployment(code, theater)
        values
            (cc, 'unassigned'       ),
            (cc, 'arctic_sea'       ),
            (cc, 'north_atlantic'   ),
            (cc, 'south_atlantic'   ),
            (cc, 'baltic_sea'       ),
            (cc, 'black_sea'        ),
            (cc, 'mediterranean_sea'),
            (cc, 'great_lakes'      ),
            (cc, 'caribbean_sea'    ),
            (cc, 'north_pacific'    ),
            (cc, 'south_pacific'    ),
            (cc, 'antarctic'        ),
            (cc, 'indian_ocean'     ),
            (cc, 'arabian_sea'      ),
            (cc, 'coastal_asia'     );

        if done then
            leave fetch_loop;
        end if;
    end loop;
end $$

create or replace procedure update_gdp()
begin
    declare cc varchar(3);
    declare done int default false;
    declare cur cursor for select code from nations;
    declare continue handler for not found set done = true;

    open cur;

    fetch_loop: loop
        fetch cur into cc;

        update nations
        set gdp=
            ( (select
               (500000*housing
                + 1000000*civil
                + 2000000*businesses)
               from buildings where code=cc)
              + (select
                 (2000000*coalesce(sum(percent*counts),0))
                 from foreign_businesses where code=cc)
              - (select
                 (     50000*coalesce(sum(infantry),      0)
                  +   400000*coalesce(sum(vehicles),      0)
                  +   200000*coalesce(sum(artillery),     0)
                  +   700000*coalesce(sum(tanks),         0)
                  +   800000*coalesce(sum(aircraft),      0)
                  +  1500000*coalesce(sum(small_vessels), 0)
                  +  5000000*coalesce(sum(submarines),    0)
                  +  6000000*coalesce(sum(destroyers),    0)
                  +  8000000*coalesce(sum(cruisers),      0)
                  + 12000000*coalesce(sum(battleships),   0)
                  + 20000000*coalesce(sum(carriers),      0))
                 from military_losses where code=cc)
              + (select coalesce(sum(gdp),0) from trade where code=cc) )
            * (1 - 0.2 * (select bloc_level from nations where code=cc))
        where code=cc;

        if done then
            leave fetch_loop;
        end if;
    end loop;
end $$

create or replace procedure update_factory_caps()
begin
    declare cc varchar(3);
    declare done int default false;
    declare cur cursor for select code from nations;
    declare continue handler for not found set done = true;

    open cur;

    read_loop: loop
        fetch cur into cc;

        update military_buildings
        set u_total=coalesce(
            5
            + (select 
               floor(
               (case
                   when (pop_core < 9)  then 0
                   when (pop_core > 66) then 57
                   else pop_core-9
               end) * @factory_scaling)
               from nations where code=cc)
            + (select
               floor(1.5*coal_plants + 1.5*oil_plants)
               from buildings where code=cc),
            @factories_min)
        where code=cc;

        update military_buildings
        set u_infantry=coalesce(
            (select
             floor(
             (case
                 when (pop_core < 9)  then 9
                 when (pop_core > 66) then 66
                 else pop_core
             end) * @infantry_scaling)
             from nations where code=cc),
            @infantry_factories_min)
        where code=cc;

        update military_buildings
        set u_naval=coalesce(
            (select
             (  infantry
              + artillery
              + tank
              + vehicle
              + aircraft) div 3
             from military_buildings where code=cc),
            0)
        where code=cc;

        if done then
            leave read_loop;
        end if;
    end loop;
end $$

create or replace procedure update_validity()
begin
    declare cc varchar(3);
    declare done int default false;
    declare cur cursor for select code from nations;
    declare continue handler for not found set done = true;

    open cur;

    read_loop: loop
        fetch cur into cc;

        if (select
               (workers < 0)
            or (food    < 0)
            or (coal    < 0)
            or (iron    < 0)
            or (oil     < 0)
            or (rubber  < 0)
            from resources where code=cc) then
            update nations set valid=false where code=cc;
        elseif (select (coal_plants > 2) or (oil_plants > 2)
                from buildings where code=cc) then
            update nations set valid=false where code=cc;
        elseif (select (  infantry
                        + artillery
                        + tank
                        + vehicle
                        + aircraft > u_total)
                    or (infantry > u_infantry)
                from military_buildings where code=cc) then
            update nations set valid=false where code=cc;
        elseif (select
                   (infantry  < 0)
                or (artillery < 0)
                or (tanks     < 0)
                or (vehicles  < 0)
                or (aircraft  < 0)
                from military_deployment
                where theater='unassigned' and code=cc) then
            update nations set valid=false where code=cc;
        elseif (select
                   (small_vessels < 0)
                or (submarines    < 0)
                or (destroyers    < 0)
                or (cruisers      < 0)
                or (battleships   < 0)
                or (carriers      < 0)
                from naval_deployment
                where theater='unassigned' and code=cc) then
            update nations set valid=false where code=cc;
        elseif (select (unassigned < 0)
                from naval_assignment where code=cc) then
            update nations set valid=false where code=cc;
        else
            update nations set valid=true where code=cc;
        end if;

        if done then
            leave read_loop;
        end if;
    end loop;
end $$

create or replace procedure update_resources()
begin
    declare cc varchar(3);
    declare done int default false;
    declare cur cursor for select code from nations;
    declare continue handler for not found set done = true;

    open cur;

    fetch_loop: loop
        fetch cur into cc;

        update resources
        set workers=
            (select (2*housing
                      *(1 + 0.25*(select
                                  floor(1.5*coal_plants + 1.5*oil_plants)
                                  from buildings where code=cc))
                     - civil
                     - businesses)
             from buildings where code=cc)
            - (select
               (  infantry
                + vehicle
                + artillery
                + tank
                + aircraft
                + naval)
               from military_buildings where code=cc)
            + (select coalesce(sum(workers),0)
               from trade where code=cc)
            + (select coalesce(sum(workers),0)
               from foreign_resources where code=cc)
        where code=cc;

        update resources
        set food=
            (select (10*farms - housing) from buildings where code=cc)
            - (select (10*infantry)
               from military_buildings where code=cc)
            + (select coalesce(sum(food),0)
               from trade where code=cc)
            + (select coalesce(sum(food),0)
               from foreign_resources where code=cc)
        where code=cc;

        update resources
        set coal=
            (select (10*coal_mines) from buildings where code=cc)
            - (select
               (  2*infantry
                + 2*vehicle
                + 2*artillery
                + 2*tank
                + 3*aircraft
                + 4*naval)
               from military_buildings where code=cc)
            + (select coalesce(sum(coal),0)
               from trade where code=cc)
            + (select coalesce(sum(coal),0)
               from foreign_resources where code=cc)
        where code=cc;

        update resources
        set iron=
            (select (10*iron_mines) from buildings where code=cc)
            - (select
               (  2*infantry
                + 2*vehicle
                + 3*artillery
                + 3*tank
                + 2*aircraft
                + 5*naval)
               from military_buildings where code=cc)
            + (select coalesce(sum(iron),0)
               from trade where code=cc)
            + (select coalesce(sum(iron),0)
               from foreign_resources where code=cc)
        where code=cc;

        update resources
        set oil=
            (select (10*oil_drills) from buildings where code=cc)
            - (select
               (  2*vehicle
                + 2*tank
                + 2*aircraft
                + 5*naval)
               from military_buildings where code=cc)
            + (select coalesce(sum(oil),0)
               from trade where code=cc)
            + (select coalesce(sum(oil),0)
               from foreign_resources where code=cc)
        where code=cc;

        update resources
        set rubber=
            (select (10*rubber_farms) from buildings where code=cc)
            - (select
               (  2*vehicle
                + 3*aircraft)
               from military_buildings where code=cc)
            + (select coalesce(sum(rubber),0)
               from trade where code=cc)
            + (select coalesce(sum(rubber),0)
               from foreign_resources where code=cc)
        where code=cc;

        if done then
            leave fetch_loop;
        end if;
    end loop;
end $$

create or replace procedure update_military_deployment()
begin
    declare cc varchar(3);
    declare done int default false;
    declare cur cursor for select code from nations;
    declare continue handler for not found set done = true;

    open cur;

    fetch_loop: loop
        fetch cur into cc;

        update military_deployment
        set infantry=
            (select
             100*infantry
             from military_buildings where code=cc)
            - (select
               sum(infantry)
               from military_deployment
               where code=cc and not theater='unassigned')
        where code=cc and theater='unassigned';

        update military_deployment
        set vehicles=
            (select
             8*vehicle
             from military_buildings where code=cc)
            - (select
               sum(vehicles)
               from military_deployment
               where code=cc and not theater='unassigned')
        where code=cc and theater='unassigned';

        update military_deployment
        set artillery=
            (select
             6*artillery
             from military_buildings where code=cc)
            - (select
               sum(artillery)
               from military_deployment
               where code=cc and not theater='unassigned')
        where code=cc and theater='unassigned';

        update military_deployment
        set tanks=
            (select
             8*tank
             from military_buildings where code=cc)
            - (select
               sum(tanks)
               from military_deployment
               where code=cc and not theater='unassigned')
        where code=cc and theater='unassigned';

        update military_deployment
        set aircraft=
            (select
             5*aircraft
             from military_buildings where code=cc)
            - (select
               sum(aircraft)
               from military_deployment
               where code=cc and not theater='unassigned')
        where code=cc and theater='unassigned';

        if done then
            leave fetch_loop;
        end if;
    end loop;
end $$

create or replace procedure update_naval_deployment()
begin
    declare cc varchar(3);
    declare done int default false;
    declare cur cursor for select code from nations;
    declare continue handler for not found set done = true;

    open cur;

    read_loop: loop
        fetch cur into cc;

        update naval_deployment
        set small_vessels=
            (select
             small_vessels
             from naval_assignment where code=cc)
            - (select
               sum(small_vessels)
               from naval_deployment
               where code=cc and not theater='unassigned')
        where code=cc and theater='unassigned';

        update naval_deployment
        set submarines=
            (select
             submarines
             from naval_assignment where code=cc)
            - (select
               sum(submarines)
               from naval_deployment
               where code=cc and not theater='unassigned')
        where code=cc and theater='unassigned';

        update naval_deployment
        set destroyers=
            (select
             destroyers
             from naval_assignment where code=cc)
            - (select
               sum(destroyers)
               from naval_deployment
               where code=cc and not theater='unassigned')
        where code=cc and theater='unassigned';

        update naval_deployment
        set cruisers=
            (select
             cruisers
             from naval_assignment where code=cc)
            - (select
               sum(cruisers)
               from naval_deployment
               where code=cc and not theater='unassigned')
        where code=cc and theater='unassigned';

        update naval_deployment
        set battleships=
            (select
             battleships
             from naval_assignment where code=cc)
            - (select
               sum(battleships)
               from naval_deployment
               where code=cc and not theater='unassigned')
        where code=cc and theater='unassigned';

        update naval_deployment
        set carriers=
            (select
             carriers
             from naval_assignment where code=cc)
            - (select
               sum(carriers)
               from naval_deployment
               where code=cc and not theater='unassigned')
        where code=cc and theater='unassigned';

        if done then
            leave read_loop;
        end if;
    end loop;
end $$

create or replace procedure update_naval_assignments()
begin
    declare cc varchar(3);
    declare done int default false;
    declare cur cursor for select code from nations;
    declare continue handler for not found set done = true;

    open cur;

    read_loop: loop
        fetch cur into cc;

        update naval_assignment
        set unassigned=
            (select 3*naval from military_buildings where code=cc)
            - (select
               (  small_vessels
                + submarines
                + destroyers
                + cruisers
                + battleships
                + carriers)
               from naval_assignment
               where code=cc)
        where code=cc;

        if done then
            leave read_loop;
        end if;
    end loop;
end $$

delimiter ;

/* triggers ================================================================ */
create trigger initialize_new
after insert on nations
for each row
call initialize_statics();

/* NOTE: `update_factory_caps()` cannot be called in a trigger here because
         it creates a trigger loop back to the nations table */
/*
create trigger update_factory_caps
after update on nations
for each row
call update_factory_caps();

create trigger update_resources_econ
after update on buildings
for each row
call update_resources();

create trigger update_resources_mil
after update on military_buildings
for each row
call update_resources();

create trigger update_resources_trade
after insert on trade
for each row
call update_resources();

create trigger update_gdp_trade
after insert on trade
for each row
call update_gdp();
*/

/* validity checking */
/*
create trigger update_validity_resources
after update on resources
for each row
call update_validity();

create trigger update_validity_buildings
after update on buildings
for each row
call update_validity();

create trigger update_validity_military_deployment
after update on military_deployment
for each row
call update_validity();

create trigger update_validity_naval_deployment
after update on naval_deployment
for each row
call update_validity();

create trigger update_validity_naval_assignment
after update on naval_deployment
for each row
call update_validity();
*/

/* NOTE: unfortunately you cannot run an update trigger on the same table
         causing the trigger (no rigorous way to prevent infinite recursion
         loops so mysql does not allow it), so the deployment functions will
         have to be called explicitly every time a user manually reassigns
         their deployment.

         an `update_whatever` function needs to be called in a trigger
         whenever a different table with relevant data updates,
         and explicitly via `call update_whatever();` on user input via js. */

/*
create trigger update_military_deployment
after update on military_buildings
for each row
call update_military_deployment();

create trigger update_naval_assignments
after update on military_buildings
for each row
call update_naval_assignments();

create trigger update_naval_deployment
after update on naval_assignment
for each row
call update_naval_deployment();
*/

/* testing ================================================================= */
insert ignore into nations(name, code)
values
    ("United States",  "USA"),
    ("Germany",        "GER"),
    ("France",         "FRA"),
    ("Sweden",         "SWE"),
    ("United Kingdom", "GBR");

update nations set pop_core=100 where code="USA";

insert ignore into trade(code, workers, food, coal, iron, oil, rubber)
values
    ("USA", 1000, 1000, 1000, 1000, 1000, 1000);

