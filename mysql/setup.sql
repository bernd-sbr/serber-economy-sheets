drop database if exists serber;

create database serber;

use serber;

-- @block table definitions
create table nations(
	code varchar(3) not null unique,
	name varchar(255) not null,
	gdp int default 0,
	pop_core int default 0,
	pop_continental int default 0,
	pop_colonial int default 0,
	bloc_level tinyint default 0,
	tier tinyint default 1,
	valid boolean default true,
	flag_url varchar(255) default '',
	primary key (code)
);

create table buildings(
	code varchar(3),
	farms      int default 0,
	coal_mines int default 0,
	iron_mines int default 0,
	housing    int default 0,
	civil      int default 0,
	businesses int default 0,
	primary key (code)
);

create table resources(
	code varchar(3),
	workers int default 0,
	food    int default 0,
	coal    int default 0,
	iron    int default 0,
	primary key (code)
);

create table strategic_resources(
	code varchar(3),
	slavery bool default true,
	yearly_bonus int default 0,
	slaves int default 0,
	sugar  int default 0,
	spices int default 0,
	cotton int default 0,
	tea    int default 0,
	coffee int default 0,
	primary key (code)
);

create table trade(
	ID int not null auto_increment,
	code varchar(3) not null,
	gdp     int,
	workers int,
	food    int,
	coal    int,
	iron    int,
	description varchar(64),
	primary key (ID)
);

create table foreign_businesses(
	ID int not null auto_increment,
	code varchar(3) not null,
	percent tinyint not null,
	counts  tinyint not null,
	primary key (ID)
);

create table foreign_resources(
	ID int not null auto_increment,
	code varchar(3) not null,
	food int,
	coal int,
	iron int,
	primary key (ID)
);

create table military_buildings(
	code varchar(3),
	max_factories int default 0,
	max_ports     int default 0,
	infantry   int default 0,
	grenadiers int default 0,
	cavalry    int default 0,
	artillery  int default 0,
	ports      int default 0,
	primary key (code)
);

create table military_assignment(
	code varchar(3),
	max_infantry int default 0,
	unassigned_infantry int default 0,
	infantry   int default 0,
	grenadiers int default 0,
	cavalry    int default 0,
	artillery  int default 0,
	primary key (code)
);

create table naval_assignment(
	code varchar(3),
	unassigned int default 0,
	tier1 int default 0,
	tier2 int default 0,
	tier3 int default 0,
	primary key (code)
);

create table military_deployment(
	code varchar(3) not null,
	theater varchar(16) not null,
	infantry   int default 0,
	grenadiers int default 0,
	cavalry    int default 0,
	artillery  int default 0,
	unique(code, theater)
);

create table naval_deployment(
	code varchar(3) not null,
	theater varchar(16) not null,
	tier1 int default 0,
	tier2 int default 0,
	tier3 int default 0,
	unique(code, theater)
);

create table military_losses(
	ID int not null auto_increment,
	code varchar(3) not null,
	infantry    int,
	grenadiers  int,
	cavalry     int,
	artillery   int,
	tier1_ships int,
	tier2_ships int,
	tier3_ships int,
	description varchar(64),
	primary key (ID)
);

-- @block functions
delimiter $$

create procedure initialize_static_fields(cc varchar(3))
begin
	insert ignore into buildings(code)           values (cc);
	insert ignore into resources(code)           values (cc);
	insert ignore into strategic_resources(code) values (cc);
	insert ignore into military_buildings(code)  values (cc);
	insert ignore into military_assignment(code) values (cc);
	insert ignore into naval_assignment(code)    values (cc);

	insert ignore into military_deployment(code, theater)
	values
		(cc, 'unassigned'     ),
		(cc, 'north_america'  ),
		(cc, 'west_coast'     ),
		(cc, 'central_america'),
		(cc, 'caribbean'      ),
		(cc, 'north_pacific'  ),
		(cc, 'south_pacific'  ),
		(cc, 'the_arctic'     ),
		(cc, 'the_antarctic'  ),
		(cc, 'western_europe' ),
		(cc, 'eastern_europe' ),
		(cc, 'middle_east'    ),
		(cc, 'central_asia'   ),
		(cc, 'east_asia'      ),
		(cc, 'south_east_asia'),
		(cc, 'antipodes'      ),
		(cc, 'india'          ),
		(cc, 'nort_africa'    ),
		(cc, 'sub_saharan'    );

	insert ignore into naval_deployment(code, theater)
	values
		(cc, 'unassigned'    ),
		(cc, 'arctic_sea'    ),
		(cc, 'north_atlantic'),
		(cc, 'south_atlantic'),
		(cc, 'baltic_sea'    ),
		(cc, 'black_sea'     ),
		(cc, 'mediterranean' ),
		(cc, 'great_lakes'   ),
		(cc, 'caribbean_sea' ),
		(cc, 'north_pacific' ),
		(cc, 'south_pacific' ),
		(cc, 'antarctic'     ),
		(cc, 'indian_ocean'  ),
		(cc, 'arabian_sea'   ),
		(cc, 'coastal_asia'  );
end $$

create procedure update_gdp(cc varchar(3))
begin
	select (0.2 * bloc_level)
	into @blockade_debuff from nations where code=cc;

	select (   500000 * housing
	        + 2000000 * civil
	        + 2000000 * businesses)
	into @econ from buildings where code=cc;

	select (2000000 * coalesce(sum(percent*counts),0))
	into @foreign_business_profits from foreign_businesses where code=cc;

	select coalesce(sum(gdp),0)
	into @trade_profits from trade where code=cc;

	select (    50000 * coalesce(sum(infantry),    0)
	        +  200000 * coalesce(sum(grenadiers),  0)
	        +  100000 * coalesce(sum(cavalry),     0)
	        +  500000 * coalesce(sum(artillery),   0)
	        + 3000000 * coalesce(sum(tier1_ships), 0)
	        + 2000000 * coalesce(sum(tier2_ships), 0)
	        + 2000000 * coalesce(sum(tier3_ships), 0))
	into @mil_losses from military_losses where code=cc;

	update nations
	set gdp = (@econ
	           + @foreign_business_profits
	           + @trade_profits ) * (1 - @blockade_debuff)
	          - @mil_losses
	where code=cc;
end $$

create procedure update_caps(cc varchar(3))
begin
	select (pop_core + 0.5 * pop_continental + 0.01 * pop_colonial)
	into @pop_inf_weighed from nations where code=cc;

	select (pop_core + 0.5 * pop_continental + 0.5 * pop_colonial)
	into @pop_fact_weighed from nations where code=cc;

	set @inf_cap = floor(@pop_inf_weighed * 40 / 100) * 100;
	if (@inf_cap < 100) then
		set @inf_cap = 100;
	elseif (@inf_cap > 1000) then
		set @inf_cap = 1000;
	end if;

	set @fact_cap = floor(@pop_fact_weighed / 100);
	if (@fact_cap < 3) then
		set @fact_cap = 3;
	elseif (@fact_cap > 25) then
		set @fact_cap = 25;
	end if;

	update military_buildings
	set max_factories = @fact_cap
	where code=cc;
	
	update military_assignment
	set max_infantry = @inf_cap
	where code=cc;

	select (infantry + grenadiers + cavalry + artillery) div 2
	into @port_cap from military_buildings where code=cc;
	set @port_cap = coalesce(@port_cap, 0);

	update military_buildings
	set max_ports = @port_cap
	where code=cc;
end $$

create procedure update_validity(cc varchar(3))
begin
	select (workers < 0) or (food < 0) or (coal < 0) or (iron < 0)
	into @resource_shortage from resources where code=cc;

	select (infantry + grenadiers + cavalry + artillery > max_factories)
	       or (grenadiers > 1) or (ports > max_ports)
	into @too_many_factories from military_buildings where code=cc;

	select tier into @country_tier from nations where code=cc;
	select grenadiers into @gren_count from military_buildings where code=cc;
	select tier1 into @t1ships from naval_assignment where code=cc;
	select tier2 into @t1ships from naval_assignment where code=cc;
	select tier3 into @t1ships from naval_assignment where code=cc;

	if (@country_tier > 1) and (@gren_count > 0) then
		set @overtier_grenadiers = true;
	end if;

	if (@country_tier > 1) and (@t1ships > 0) then
		set @overtier_ships = true;
	elseif (@country_tier > 2) and (@t2ships > 0) then
		set @overtier_ships = true;
	end if;

	select (infantry < 0) or (grenadiers < 0)
	       or (cavalry < 0) or (artillery < 0)
	into @land_overdeployment
	from military_deployment where theater='unassigned' and code=cc;

	select (unassigned < 0)
	into @naval_overassignment from naval_assignment where code=cc;

	select (tier1 < 0) or (tier2 < 0) or (tier3 < 0)
	into @naval_overdeployment
	from naval_deployment where theater='unassigned' and code=cc;

	if (   @resource_shortage
	    or @too_many_factories
	    or @overtier_grenadiers
	    or @overtier_ships
	    or @land_overdeployment
	    or @naval_overassignment
	    or @naval_overdeployment) then
		update nations set valid=false where code=cc;
	else
		update nations set valid=true where code=cc;
	end if;
end $$

create procedure update_resources(cc varchar(3))
begin
	select (2*housing - civil - businesses)
	into @worker_production from buildings where code=cc;

	select (infantry + grenadiers + cavalry + artillery + ports)
	into @employment from military_buildings where code=cc;

	select coalesce(sum(workers),0)
	into @foreign_workers from trade where code=cc;

	select (10*farms - housing)
	into @food_production from buildings where code=cc;

	select (10 * infantry + 10 * grenadiers + 20 * cavalry)
	into @food_consumption from military_buildings where code=cc;

	select coalesce(sum(food),0)
	into @traded_food from trade where code=cc;

	select coalesce(sum(food),0)
	into @foreign_food from foreign_resources where code=cc;

	select (10*coal_mines)
	into @coal_production from buildings where code=cc;

	select (2*infantry + 3*grenadiers + 3*cavalry + 2*artillery + 3*ports)
	into @coal_use from military_buildings where code=cc;

	select coalesce(sum(coal),0)
	into @traded_coal from trade where code=cc;

	select coalesce(sum(coal),0)
	into @foreign_coal from foreign_resources where code=cc;

	select (10*iron_mines)
	into @iron_production from buildings where code=cc;

	select (2*infantry + 3*grenadiers + 3*cavalry + 4*artillery + 6*ports)
	into @iron_use from military_buildings where code=cc;

	select coalesce(sum(iron),0)
	into @traded_iron from trade where code=cc;

	select coalesce(sum(iron),0)
	into @foreign_iron from foreign_resources where code=cc;

	update resources
	set workers = @worker_production + @foreign_workers - @employment,
	    food = @food_production + @traded_food + @foreign_food
	           - @food_consumption,
	    coal = @coal_production + @traded_coal + @foreign_coal
	           - @coal_use,
	    iron = @iron_production + @traded_iron + @foreign_iron
	           - @iron_use
	where code=cc;
end $$

create procedure update_military_assignment(cc varchar(3))
begin
	select max_infantry
	into @inf_remaining from military_assignment where code=cc;

	select 100 * infantry,
	       100 * grenadiers,
	        25 * cavalry,
	         4 * artillery
	into @produced_infantry,
	     @produced_grenadiers,
	     @produced_cavalry,
	     @produced_artillery
	from military_buildings
	where code=cc;

	if (@inf_remaining > @produced_cavalry) then
		update military_assignment
		set cavalry=@produced_cavalry
		where code=cc;
		set @inf_remaining = @inf_remaining - @produced_cavalry;
	else
		update military_assignment
		set cavalry=@inf_remaining
		where code=cc;
		set @inf_remaining = 0;
	end if;

	if (@inf_remaining > @produced_grenadiers) then
		update military_assignment
		set grenadiers=@produced_grenadiers
		where code=cc;
		set @inf_remaining = @inf_remaining - @produced_cavalry;
	else
		update military_assignment
		set grenadiers=@inf_remaining
		where code=cc;
		set @inf_remaining = 0;
	end if;

	if (@inf_remaining > @produced_infantry) then
		update military_assignment
		set infantry=@produced_infantry
		where code=cc;
		set @inf_remaining = @inf_remaining - @produced_cavalry;
	else
		update military_assignment
		set infantry=@inf_remaining
		where code=cc;
		set @inf_remaining = 0;
	end if;

	update military_assignment
	set unassigned_infantry=@inf_remaining, artillery=@produced_artillery
	where code=cc;
end $$

create procedure update_naval_assignment(cc varchar(3))
begin
	select (tier1 + tier2 + tier3)
	into @assigned_navy from naval_assignment where code=cc;

	select (4 * ports)
	into @produced_navy from military_buildings where code=cc;

	update naval_assignment
	set unassigned = @produced_navy - @assigned_navy
	where code=cc;
end $$

create procedure update_military_deployment(cc varchar(3))
begin
	select sum(infantry), sum(grenadiers), sum(cavalry), sum(artillery)
	into @deployed_infantry,
	     @deployed_grenadiers,
	     @deployed_cavalry,
	     @deployed_artillery
	from military_deployment
	where not theater='undeployed' and code=cc;

	select infantry, grenadiers, cavalry, artillery
	into @assigned_infantry,
	     @assigned_grenadiers,
	     @assigned_cavalry,
	     @assigned_artillery
	from military_buildings
	where code=cc;

	update military_deployment
	set infantry   = @assigned_infantry   - @deployed_infantry,
	    grenadiers = @assigned_grenadiers - @deployed_grenadiers,
	    cavalry    = @assigned_cavalry    - @deployed_cavalry,
	    artillery  = @assigned_artillery  - @deployed_artillery
	where code=cc and theater='undeployed';
end $$

create procedure update_naval_deployment(cc varchar(3))
begin
	select sum(tier1), sum(tier2), sum(tier3)
	into @deployed_tier1, @deployed_tier2, @deployed_tier3
	from naval_deployment
	where not theater='unassigned' and code=cc;

	select sum(tier1), sum(tier2), sum(tier3)
	into @assigned_tier1, @assigned_tier2, @assigned_tier3
	from naval_assignment
	where code=cc;

	update naval_deployment
	set tier1 = @assigned_tier1 - @deployed_tier1,
	    tier2 = @assigned_tier2 - @deployed_tier2,
	    tier3 = @assigned_tier3 - @deployed_tier3
	where theater='unassigned' and code=cc;
end $$

create procedure update_strat_resources(cc varchar(3))
begin
	select (  100000 * slaves
	        + 100000 * sugar
	        +  80000 * spices
	        +  50000 * cotton
	        +  40000 * tea
	        +  40000 * coffee),
	        slavery
	into @strat_resource_profits, @slavery_lawful
	from strategic_resources where code=cc;

	if not @slavery_lawful then
		set @strat_resource_profits = @strat_resource_profits div 2;
	end if;

	update strategic_resources
	set yearly_bonus = @strat_resource_profits
	where code=cc;
end $$

delimiter ;

-- @block triggers
create trigger setup_new
after insert on nations
for each row
call initialize_static_fields(new.code);

-- @block testing
insert ignore into nations(name, code)
values
    ("United States",  "USA"),
    ("Germany",        "GER"),
    ("France",         "FRA"),
    ("Sweden",         "SWE"),
    ("United Kingdom", "GBR");

update nations set pop_core=100 where code="USA";

insert ignore into trade(code, workers, food, coal, iron)
values
    ("USA", 1000, 1000, 1000, 1000);

